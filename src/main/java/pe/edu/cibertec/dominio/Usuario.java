package pe.edu.cibertec.dominio;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pe.edu.cibertec.dominio.base.EntidadBase;

public class Usuario extends EntidadBase {

    private String usuario;
    private String clave;
    private boolean enabled;
    private Set<UsuarioRol> usuarioRol = new HashSet<UsuarioRol>(0);
        
    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }
    public boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    public Set<UsuarioRol> getUsuarioRol() {
        return usuarioRol;
    }
    public void setUsuarioRol(Set<UsuarioRol> usuarioRol) {
        this.usuarioRol = usuarioRol;
    }    
}
