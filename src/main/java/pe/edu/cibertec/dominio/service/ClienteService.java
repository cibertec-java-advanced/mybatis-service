package pe.edu.cibertec.dominio.service;

import pe.edu.cibertec.dominio.Cliente;

public interface ClienteService {
    
    Cliente buscarPorUsuario(Long usuarioId);
}
