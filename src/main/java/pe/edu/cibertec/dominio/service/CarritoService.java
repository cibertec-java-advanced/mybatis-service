package pe.edu.cibertec.dominio.service;

import java.util.List;
import pe.edu.cibertec.dominio.Carrito;
import pe.edu.cibertec.dominio.DetalleCarrito;

public interface CarritoService {
    
    void crearCarrito(Carrito carrito, List<DetalleCarrito> detalleCarrito);
}
