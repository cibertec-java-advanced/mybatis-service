package pe.edu.cibertec.dominio.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.cibertec.dominio.Carrito;
import pe.edu.cibertec.dominio.DetalleCarrito;
import pe.edu.cibertec.dominio.service.CarritoService;
import pe.edu.cibertec.repositorio.CarritoRepositorio;

@Service
public class CarritoServiceImpl implements CarritoService{
    
    @Autowired
    private CarritoRepositorio carritoRepositorio;
    
    @Override
    public void crearCarrito(Carrito carrito, List<DetalleCarrito> detalleCarrito) {
        carritoRepositorio.crearCarrito(carrito, detalleCarrito);
    }
    
}
