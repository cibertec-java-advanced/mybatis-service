package pe.edu.cibertec.dominio.service;

import java.util.List;
import pe.edu.cibertec.dominio.Producto;

public interface ProductoService {
    List<Producto> obtenerTodos();
}
