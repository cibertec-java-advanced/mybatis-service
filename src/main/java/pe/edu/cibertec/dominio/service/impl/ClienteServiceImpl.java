package pe.edu.cibertec.dominio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.cibertec.dominio.Cliente;
import pe.edu.cibertec.dominio.service.ClienteService;
import pe.edu.cibertec.repositorio.ClienteRepositorio;

@Service
public class ClienteServiceImpl implements ClienteService{
    
    @Autowired
    private ClienteRepositorio clienteRepositorio;
    
    @Override
    public Cliente buscarPorUsuario(Long usuarioId) {
        return clienteRepositorio.buscarPorUsuario(usuarioId);
    }    
}
