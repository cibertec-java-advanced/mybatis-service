package pe.edu.cibertec.dominio.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.cibertec.dominio.Producto;
import pe.edu.cibertec.dominio.service.ProductoService;
import pe.edu.cibertec.repositorio.ProductoRepositorio;

@Service
public class ProductoServiceImpl implements ProductoService{

    @Autowired
    private ProductoRepositorio productoRepositorio;
    
    @Override
    public List<Producto> obtenerTodos() {
        return productoRepositorio.obtenerTodos();
    }    
}
