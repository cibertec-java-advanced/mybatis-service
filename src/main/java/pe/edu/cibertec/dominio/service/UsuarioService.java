package pe.edu.cibertec.dominio.service;

import java.util.List;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.dominio.UsuarioRol;

public interface UsuarioService {
    
    Usuario iniciarSesion(String usuario, String clave);
    Usuario buscarUsuarioPorUsuario(String usuario);
    List<UsuarioRol> buscarUsuarioRolPorUsuario(String usuario);
}
