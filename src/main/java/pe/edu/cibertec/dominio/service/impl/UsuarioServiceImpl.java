package pe.edu.cibertec.dominio.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.dominio.UsuarioRol;
import pe.edu.cibertec.dominio.service.UsuarioService;
import pe.edu.cibertec.repositorio.UsuarioRepositorio;

@Service
public class UsuarioServiceImpl implements UsuarioService{
    
    @Autowired
    private UsuarioRepositorio usuarioRepositorio;
    
    @Override
    public Usuario iniciarSesion(String usuario, String clave) {
        return usuarioRepositorio.iniciarSesion(usuario, clave);
    }    

    @Override
    public Usuario buscarUsuarioPorUsuario(String usuario) {
        Usuario user = usuarioRepositorio.buscarUsuarioPorUsuario(usuario);
        List<UsuarioRol> usuarioRoles = this.buscarUsuarioRolPorUsuario(usuario);
        Set<UsuarioRol> usuarioRol = new HashSet<UsuarioRol>(0);
        
        usuarioRoles.forEach(u -> {
            UsuarioRol ur = new UsuarioRol();
            ur.setId(u.getId());
            ur.setRol(u.getRol());
            ur.setUsuario(u.getUsuario());
            usuarioRol.add(ur);
        });
        user.setUsuarioRol(usuarioRol);
        return user;
    }

    @Override
    public List<UsuarioRol> buscarUsuarioRolPorUsuario(String usuario) {
        return usuarioRepositorio.buscarUsuarioRolPorUsuario(usuario);
    }
}
