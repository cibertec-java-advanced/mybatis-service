package pe.edu.cibertec.dominio;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import pe.edu.cibertec.dominio.base.EntidadBase;

public class Cliente extends EntidadBase {
    
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private Date fechaNacimiento;
    private Usuario usuario;
    private transient Integer edad;

    public void despuesCargarEntidad() {
        LocalDate ahora = LocalDate.now();
        LocalDate fecha = this.fechaNacimiento.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        edad = (int)fecha.until(ahora, ChronoUnit.YEARS);
    }
    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }
    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    public Integer getEdad() {
        return edad;
    }
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
    public Usuario getUsuario() {
        return usuario;
    }
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
