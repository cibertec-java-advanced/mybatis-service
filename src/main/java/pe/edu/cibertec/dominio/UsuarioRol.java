package pe.edu.cibertec.dominio;

import pe.edu.cibertec.dominio.base.EntidadBase;

public class UsuarioRol extends EntidadBase{
    
    private Usuario usuario;
    private String rol;

    public Usuario getUsuario() {
        return usuario;
    }
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    public String getRol() {
        return rol;
    }
    public void setRol(String rol) {
        this.rol = rol;
    }   
}
