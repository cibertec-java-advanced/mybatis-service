package pe.edu.cibertec.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.dominio.UsuarioRol;
import pe.edu.cibertec.repositorio.UsuarioRepositorio;

@Configuration
@ComponentScan("pe.edu.cibertec")
public class PrincipalMybatis {

    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();

        Resource[] archivosPropiedades = new Resource[] {
            new ClassPathResource("database.properties")
        };
        pspc.setLocations(archivosPropiedades);
        pspc.setIgnoreUnresolvablePlaceholders(true);
        return pspc;
    }

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(PrincipalMybatis.class);
        
        UsuarioRepositorio usuarioRepo = ctx.getBean(UsuarioRepositorio.class);
        
        Usuario usuario1 = new Usuario();
        Usuario usuario2 = new Usuario();
        usuario1 = usuarioRepo.iniciarSesion("lfalero", "123123123");
        usuario2 = usuarioRepo.iniciarSesion("lfalero", "123456");
        System.out.println("INICIO DE SESION OK: " + usuario1.getUsuario());
        System.out.println("INICIO DE SESION ERROR: " + usuario2);
        
        Usuario usuario = new Usuario();
        List<UsuarioRol> usuarioRoles = usuarioRepo.buscarUsuarioRolPorUsuario("lfalero");
        Set<UsuarioRol> usuarioRol = new HashSet<UsuarioRol>(0);
        usuario = usuarioRepo.buscarUsuarioPorUsuario("lfalero");
        
        usuarioRoles.forEach(u -> {
            UsuarioRol ur = new UsuarioRol();
            ur.setId(u.getId());
            ur.setRol(u.getRol());
            ur.setUsuario(u.getUsuario());
            usuarioRol.add(ur);
        });
        usuario.setUsuarioRol(usuarioRol);
        
        System.out.println(
            "USUARIO-> "+
            usuario.getId()+" - "+
            usuario.getUsuario()+" - "+
            usuario.getClave()+" - "+
            usuario.isEnabled()
        );
        usuario.getUsuarioRol().forEach(u -> System.out.println(
            "ROLES-> "+
            u.getId() +" - "+
            u.getUsuario().getId() +" - "+
            u.getRol()
        ));
    } 
}
