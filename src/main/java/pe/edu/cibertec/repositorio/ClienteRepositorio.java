package pe.edu.cibertec.repositorio;

import pe.edu.cibertec.dominio.Cliente;

public interface ClienteRepositorio {

    Cliente buscarPorUsuario(Long usuarioId);
}
