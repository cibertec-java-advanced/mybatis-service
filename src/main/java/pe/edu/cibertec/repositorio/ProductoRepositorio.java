package pe.edu.cibertec.repositorio;

import java.util.List;
import pe.edu.cibertec.dominio.Producto;

public interface ProductoRepositorio {

    List<Producto> obtenerTodos();
}
