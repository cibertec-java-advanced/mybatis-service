package pe.edu.cibertec.repositorio.impl;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.dominio.UsuarioRol;
import pe.edu.cibertec.repositorio.UsuarioRepositorio;
import pe.edu.cibertec.repositorio.mapper.UsuarioMapper;
import pe.edu.cibertec.repositorio.mapper.UsuarioRolMapper;

@Repository
public class UsuarioMybatisRepositorioImpl implements UsuarioRepositorio{
    
    @Autowired
    private UsuarioMapper usuarioMapper;
    
    @Autowired
    private UsuarioRolMapper usuarioRolMapper;

    @Override
    public Usuario iniciarSesion(String usuario, String clave) {
        Usuario us = new Usuario();
        us.setUsuario(usuario);
        us.setClave(clave);
        return usuarioMapper.iniciarSesion(us);
    }    

    @Override
    public Usuario buscarUsuarioPorUsuario(String usuario) {
        Usuario us = new Usuario();
        us.setUsuario(usuario);
        return usuarioMapper.buscarUsuarioPorUsuario(us);
    }

    @Override
    public List<UsuarioRol> buscarUsuarioRolPorUsuario(String usuario) {
        Usuario us = new Usuario();
        us.setUsuario(usuario);
        
        UsuarioRol ur = new UsuarioRol();
        ur.setUsuario(us);
        return usuarioRolMapper.buscarUsuarioRolPorUsuario(ur);
    }
}
