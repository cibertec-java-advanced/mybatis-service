package pe.edu.cibertec.repositorio.impl;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.edu.cibertec.dominio.Cliente;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.repositorio.ClienteRepositorio;
import pe.edu.cibertec.repositorio.mapper.ClienteMapper;

@Repository
public class ClienteMybatisRepositorioImpl implements ClienteRepositorio{
    
    @Autowired
    private ClienteMapper clienteMapper;

    @Override
    public Cliente buscarPorUsuario(Long usuarioId) {
        Usuario usuario = new Usuario();
        usuario.setId(usuarioId);
        
        Cliente cliente = new Cliente();
        cliente.setUsuario(usuario);
        
        return clienteMapper.buscarPorUsuario(cliente);
    }    
}
