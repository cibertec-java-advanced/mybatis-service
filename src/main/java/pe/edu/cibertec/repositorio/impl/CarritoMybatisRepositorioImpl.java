package pe.edu.cibertec.repositorio.impl;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.edu.cibertec.dominio.Carrito;
import pe.edu.cibertec.dominio.DetalleCarrito;
import pe.edu.cibertec.repositorio.CarritoRepositorio;
import pe.edu.cibertec.repositorio.mapper.CarritoMapper;
import pe.edu.cibertec.repositorio.mapper.DetalleCarritoMapper;

@Repository
public class CarritoMybatisRepositorioImpl implements CarritoRepositorio{
    
    @Autowired
    private CarritoMapper carritoMapper;
    
    @Autowired
    private DetalleCarritoMapper detalleCarritoMapper;

    @Override
    public void crearCarrito(Carrito carrito, List<DetalleCarrito> detalleCarrito) {
        carritoMapper.registrarCarrito(carrito);        
        detalleCarrito.forEach(data -> {
            data.setCarritoCompras(carrito);
            detalleCarritoMapper.registrarDetalleCarrito(data);
        });
    }    
}
