package pe.edu.cibertec.repositorio.impl;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.edu.cibertec.dominio.Producto;
import pe.edu.cibertec.repositorio.ProductoRepositorio;
import pe.edu.cibertec.repositorio.mapper.ProductoMapper;

@Repository
public class ProductoMybatisRepositorioImpl implements ProductoRepositorio{
    
    @Autowired
    private ProductoMapper productoMapper;
    
    @Override
    public List<Producto> obtenerTodos() {
        return productoMapper.obtenerTodos();
    }    
}
