package pe.edu.cibertec.repositorio.mapper;

import pe.edu.cibertec.dominio.Cliente;

public interface ClienteMapper {

    Cliente buscarPorUsuario(Cliente cliente);
}
