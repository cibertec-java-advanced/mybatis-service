package pe.edu.cibertec.repositorio.mapper;

import pe.edu.cibertec.dominio.Usuario;

public interface UsuarioMapper {

    Usuario iniciarSesion(Usuario usuario);
    Usuario buscarUsuarioPorUsuario(Usuario usuario);
}
