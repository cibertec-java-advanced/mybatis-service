package pe.edu.cibertec.repositorio.mapper;

import java.util.List;
import pe.edu.cibertec.dominio.Producto;

public interface ProductoMapper {

    List<Producto> obtenerTodos();
}
