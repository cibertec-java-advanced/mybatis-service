package pe.edu.cibertec.repositorio.mapper;

import java.util.List;
import pe.edu.cibertec.dominio.UsuarioRol;

public interface UsuarioRolMapper {
    List<UsuarioRol> buscarUsuarioRolPorUsuario(UsuarioRol usuarioRol);
}
