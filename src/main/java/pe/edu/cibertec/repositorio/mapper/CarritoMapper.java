package pe.edu.cibertec.repositorio.mapper;

import pe.edu.cibertec.dominio.Carrito;

public interface CarritoMapper {

    Long registrarCarrito(Carrito carrito);
}
