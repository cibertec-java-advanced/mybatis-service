package pe.edu.cibertec.repositorio.mapper;

import pe.edu.cibertec.dominio.DetalleCarrito;

public interface DetalleCarritoMapper {

    void registrarDetalleCarrito(DetalleCarrito detalleCarrito);
}
