package pe.edu.cibertec.repositorio;

import java.util.List;
import pe.edu.cibertec.dominio.Carrito;
import pe.edu.cibertec.dominio.DetalleCarrito;

public interface CarritoRepositorio {

    void crearCarrito(Carrito carrito, List<DetalleCarrito> detalleCarrito);
}
