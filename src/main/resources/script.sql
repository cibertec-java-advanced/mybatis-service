drop database if exists tienda;

create database tienda;

use tienda;

create table usuario(
    id bigint primary key auto_increment,
    usuario varchar(100) not null,
    clave varchar(100) not null,
    enabled int default 0
);

create table usuario_rol(
    id bigint primary key auto_increment,
    usuario_id bigint references usuario(id),
    rol varchar(100) not null
);

create table cliente(
    id bigint primary key auto_increment,
    nombre varchar(100) not null,
    apellido_paterno varchar(100) not null,
    apellido_materno varchar(100) not null,
    fecha_nacimiento date null,
    usuario_id bigint references usuario(id)
);

create table categoria(
  id bigint primary key auto_increment,
  nombre varchar(100) not null
);

create table marca(
  id bigint primary key auto_increment,
  nombre varchar(100) not null
);

create table tab_producto (
  id bigint primary key auto_increment,
  nombre varchar(100) not null,
  descripcion varchar(500),
  precio decimal(10,2) not null,
  imagen blob,
  categoria_id bigint references categoria(id),
  marca_id bigint references marca(id)
);

create table carrito (
  id bigint primary key auto_increment, 
  cantidad int not null,
  total decimal(10,2) not null,
  fecha_compra datetime,
  cliente_id bigint references cliente(id)
);

create table detalle_carrito (
  id bigint primary key auto_increment,  
  cantidad int not null,
  precio_unitario decimal(10,2) not null,
  carrito_id bigint references carrito(id),
  producto_id bigint references tab_producto(id)
);

insert into usuario values (null, 'lfalero', '$2a$10$6LtLrwJxnZ6GTsjNUNc8nuud.m9R5FEAIVflEOWKKZoi7W4mULdKy', 1);

insert into usuario_rol values (null, 1, 'ROLE_ADMIN');

insert into cliente values (null, 'Luis', 'Falero', 'Otiniano', '1994-04-14', 1);

insert into categoria values
(null, 'Electrodomésticos'),
(null, 'Juguetería'),
(null, 'Deportes');

insert into marca values
(null, 'LG'),
(null, 'IKEA'),
(null, 'Sony'),
(null, 'Saga Falabella');

insert into tab_producto values
(null, 'Refrigeradora LG H230', 'Refrigeradora LG H230 enfría más que su corazón', 700, null, 1, 1),
(null, 'Lavadora LG S400', 'Lavadora LG S400', 640, null, 1, 1),
(null, 'Cocina IKEA Gris 852', 'Cocina IKEA Gris 852', 970, null, 1, 2),
(null, 'Playstation 4 Slim SSD 120 GB', 'Playstation 4 Slim SSD 120 GB', 2500, null, 2, 3),
(null, 'Peluche Oso Panda Cariñositos', 'Peluche Oso Panda Cariñositos', 35, null, 2, 4),
(null, 'Mesa de ping pong 220 x 90', 'Mesa de ping pong 220 x 90', 357, null, 2, 4),
(null, 'Faja corredora 14 velocidades', 'Faja corredora 14 velocidades', 1500, null, 3, 4),
(null, 'Faja corredora 8 velocidades', 'Faja corredora 8 velocidades', 950, null, 3, 4);
